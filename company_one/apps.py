from django.apps import AppConfig


class CompanyOneConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'company_one'
