from django.http import HttpResponse

from company_one.models import CompanyOne


def index(request):
    data = CompanyOne.objects.get()
    return HttpResponse("Hello, from  " + str(data.name) + "  database")
