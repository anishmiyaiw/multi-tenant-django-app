from django.contrib import admin
from company_one.models import CompanyOne


@admin.register(CompanyOne)
class CompanyOneAdmin(admin.ModelAdmin):
    list_display = ('name',)
