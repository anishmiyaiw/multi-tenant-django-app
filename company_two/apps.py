from django.apps import AppConfig


class CompanyTwoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'company_two'
