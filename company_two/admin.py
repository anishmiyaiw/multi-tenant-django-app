from django.contrib import admin
from company_two.models import CompanyTwo


@admin.register(CompanyTwo)
class CompanyTwoAdmin(admin.ModelAdmin):
    list_display = ('name',)
